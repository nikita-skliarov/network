from django.core.management import BaseCommand

from ..seeds.run import NetworkSeeds


class Command(BaseCommand):
    def handle(self, *args, **options):
        try:
            network_seeds = NetworkSeeds()
            network_seeds.run_seeds()
            self.stdout.write(
                self.style.SUCCESS('database was seeded!')
            )
        except Exception as e:
            print(e)
