from django.contrib.auth.models import User
from django.db import models


class Post(models.Model):
    title = models.CharField(max_length=200)
    users_liked = models.ManyToManyField(User, blank=True)
    image = models.ImageField(upload_to='images', default='default.png')
    author = models.ForeignKey(User, related_name="posts", on_delete=models.CASCADE, null=True)
    author_name = models.CharField(max_length=200, null=True)
    like_count = models.IntegerField(default=0)
    content = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)

    def like(self, user):
        self.like_count += 1
        self.users_liked.add(user)
        self.save()

    def dislike(self, user):
        self.like_count -= 1
        self.users_liked.remove(user)
        self.save()
