import React, {Component, Fragment} from 'react';
import ReactDOM from 'react-dom';
import {HashRouter as Router, Route, Switch} from 'react-router-dom';
import {Provider} from "react-redux";
import store from "../store";
import Header from "./layout/Header";
import Login from "./accounts/Login";
import Register from "./accounts/Register";
import AuthorizedRoute from "./common/AuthorizedRoute";
import {loadUser} from "../actions/auth";
import Footer from "./layout/Footer";
import StartPage from "./layout/StartPage";
import Posts from "./posts/Posts";
import Form from "./posts/Form";


class App extends Component {
    componentDidMount() {
        store.dispatch(loadUser);
    }

    render() {
        return (
            <Provider store={store}>
                    <Router>
                        <Fragment>
                            <Header/>
                            <div className="container">
                                <Switch>
                                    <Route exact path="/" component={StartPage}/>
                                    <AuthorizedRoute exact path="/posts" component={Posts}/>
                                    <AuthorizedRoute exact path="/posts/create" component={Form}/>
                                    <Route exact path="/register" component={Register}/>
                                    <Route exact path="/login" component={Login}/>
                                </Switch>
                            </div>
                            <Footer/>
                        </Fragment>
                    </Router>
            </Provider>
        )
    }
}

ReactDOM.render(<App/>, document.getElementById("app"))