import React from 'react';

function Footer() {
    return (
        <footer>
            <div className="container">
                <div className="row">
                    <div className="col text-center py-3">
                        &copy; Nikita Skliarov
                    </div>
                </div>
            </div>
        </footer>
    );
}

export default Footer;