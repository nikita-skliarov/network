import React, {Component} from 'react';
import {Link} from "react-router-dom";

class StartPage extends Component {
    render() {
        return (
            <div style={{textAlign: "center", height: 400, marginTop:200}}>
                <h1>Hi! It's simple REST application!</h1>
                <p>Created by using DRF, Django, django-knox, React and Redux.</p>
                <Link to="/register"><button className="btn btn-primary">Let's Go</button></Link>
            </div>
        );
    }
}

export default StartPage;