import axios from "axios";
import {GET_POSTS, DELETE_POST, ADD_POST} from "./types";
import {getConfig} from "./auth";


export const getPosts = () => (dispatch, getState) => {
    axios.get('api/posts/', getConfig(getState))
        .then(res => {
            dispatch({
                type: GET_POSTS,
                payload: res.data
            })
        })
        .catch(err => console.log(err.response.data))
}

export const getUserPosts = () => (dispatch, getState) => {
    axios.get('api/posts/self/', getConfig(getState))
        .then(res => {
            dispatch({
                type: GET_POSTS,
                payload: res.data
            })
        })
        .catch(err => console.log(err.response.data))
}

export const deletePost = (id) => (dispatch, getState) => {
    axios.delete(`api/posts/${id}/`, getConfig(getState))
        .then(res => {
            dispatch({
                type: DELETE_POST,
                payload: id
            })
        })
        .catch(err => console.log(err.response.data))
}

export const addPost = (post) => (dispatch, getState) => {
    let formData = new FormData()
    formData.append("title", post.title)
    if (post.image) {
        formData.append("image", post.image)
    }
    formData.append("content", post.content)
    let config = getConfig(getState)
    config.headers['Content-Type'] = 'multipart/form-data'
    axios.post('api/posts/', formData, config)
        .then(res => {
            dispatch({
                type: ADD_POST,
                payload: res.data
            });
        })
        .catch(err => console.log(err.response.data));
};

export const likePost = (id) => (dispatch, getState) => {
    axios.post(`api/posts/${id}/like/`, null, getConfig(getState))
        .then(res => {
            console.log("Post Liked")
        })
        .catch(err => console.log(err.response.data))
}

export const dislikePost = (id) => (dispatch, getState) => {
    axios.delete(`api/posts/${id}/dislike/`, getConfig(getState))
        .then(res => {
            console.log("Post Disliked")
        })
        .catch(err => console.log(err.response.data))
}