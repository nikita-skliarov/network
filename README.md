# Network Application

Simple REST Application using Django, DRF, Django-knox-token, React, Redux.

--------

## Installing dependencies
For `frontend`:
```
npm install
```
For `backend`:
```
pipenv shell
pipenv install --skip-lock
```

## Build
For `frontend`:
```
npm run build
```
For `backend`:
```
python3 manage.py migrate
python3 manage.py runserver
```

## Seeds
```
python3 manage.py run_seeds
```